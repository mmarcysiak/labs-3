﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class CPU
    {
        private Bateria battery;
        private Czujniki receptors;
        private Memory pamiec;

        public CPU()
        {
            battery = new Bateria();
            receptors = new Czujniki();
            pamiec = new Memory();
        }
        public void Management()
        {
            Console.WriteLine(battery.SprawdzStan_i_Laduj());
            Console.WriteLine(receptors.Skanuj());
            Console.WriteLine(pamiec.Sprawdz_Stan());
        }
        public static object Metoda_Baterii(object impl)
        {
            return new Bateria();
        }
        public static object Metoda_czujnikow(object impl)
        {
            return new Czujniki();
        }
        public static object Metoda_Pamieci(object impl)
        {
            return new Memory();
        }
    }
    public static class Domieszka
    {
        public static void Stan( this IBateria target)
        {
            Console.WriteLine("Bateria jest w pelni naladowana");
        }
    }
}
