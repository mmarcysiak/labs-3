﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Bateria:IBateria
    {
        //private bool Stan;
        public string SprawdzStan_i_Laduj()
        {
            var rand = new Random();
            var los = rand.NextDouble();
            if (los >= 0.5)
            {
                return "Bateria sprawna";
            }
            else
            {
                return "Bateria niesprawna";
            }
        }
    }
    public class Czujniki : ISkanowanie
    {
        public string Skanuj()
        {
            var rand = new Random();
            var los = rand.NextDouble();
            if (los >= 0.5)
            {
                return "Skanowanie";
            }
            else
            {
                return "czujniki nie dzialaja";
            }
        }
    }
    public class Memory : IPamiec
    {
        public string Sprawdz_Stan()
        {
            var rand = new Random();
            var los = rand.NextDouble();
            if (los >= 0.5)
            {
                return "Pamiec sprawna";
            }
            else
            {
                return "Pamiec niesprawna";
            }
        }
    }

}
