﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Contract
{
    public interface IBateria
    {
       string SprawdzStan_i_Laduj();
    }
    public interface ISkanowanie
    {
        string Skanuj();
    }
    public interface IPamiec
    {
        string Sprawdz_Stan();
    }
}
