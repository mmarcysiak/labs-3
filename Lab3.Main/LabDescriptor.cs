﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(IBateria);
        public static Type I2 = typeof(ISkanowanie);
        public static Type I3 = typeof(IPamiec);

        public static Type Component = typeof(CPU);

        public delegate object GetInstance(object component);

        public static GetInstance GetInstanceOfI1 = CPU.Metoda_Baterii;
        public static GetInstance GetInstanceOfI2 = CPU.Metoda_czujnikow;
        public static GetInstance GetInstanceOfI3 = CPU.Metoda_Pamieci;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Domieszka);
        public static Type MixinFor = typeof(IBateria);

        #endregion
    }
}
